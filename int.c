//////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 02a - Datatypes
//
// @file int.c
// @version 1.0
//
// Print the characteristics of the "int" and "unsigned int" datatypes
//
// @author Destynee Fagaragan <djaf6@hawaii.edu>
// @brief Lab 02 - Datatypes - EE 205 - Spr 2021
// @date 01.25.2021
// ///////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"

void doInt(){
   printf(TABLE_FORMAT_INT, "int", sizeof(int)*16, sizeof(int), INT_MIN, INT_MAX);
}

void flowInt(){
   int overflow = INT_MAX;
   printf("int overflow: %d + 1", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = INT_MIN;
   printf("int underflow: %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}

void doUnsignedInt(){
   printf(TABLE_FORMAT_UINT, "unsigned int", sizeof(unsigned int)*16, sizeof(unsigned int), 0, UINT_MAX);
}

void flowUnsignedInt(){
   unsigned int overflow = UINT_MAX;
   printf("unsigned int overflow: %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   unsigned int underflow = 0;
   printf("unsigned int underflow: %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);

}
