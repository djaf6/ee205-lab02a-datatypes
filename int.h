//////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 02a - Datatypes
//
// @file int.h
// @version 1.0
//
// Print the characteristics of the "int" and "unsigned int" datatypes
//
// @author Destynee Fagaragan <djaf6@hawaii.edu>
// @brief Lab 02 - Datatypes - EE 205 - Spr 2021
// @date 01.25.2021
///////////////////////////////////////////////////////

extern void doInt();
extern void flowInt();

extern void doSignedInt();
extern void flowSIgnedInt();

extern void doUnsignedInt();
extern void flowUnsignedInt();


