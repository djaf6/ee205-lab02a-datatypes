///////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 02a - Datatypes
//
// @file long.h
// @version 1.0
//
// Print the characteristics of the "long" and "unsigned long" datatypes
//
// @author Destynee Fagaragan <djaf6@hawaii.edu>
// @brief Lab 02 - Datatypes - EE 205 - Spr 2021
// @date 01/25/2021
///////////////////////////////////////////////////////////////////

extern void doLong();
extern void flowLong();

extern void doSignedLong();
extern void flowSignedLong();

extern void doUnsignedLong();
extern void flowUnsignedLong();
